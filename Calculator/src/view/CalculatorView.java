package view;

import controller.CalculatorController;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static javafx.scene.text.Font.font;
import static javafx.scene.text.Font.font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalculatorView extends JFrame {

    CalculatorController cc;

    public static JButton zero = new JButton("0");
    public static JButton plus = new JButton("+");
    public static JButton minus = new JButton("-");
    public static JButton clearAll = new JButton("C");
    public static JButton equals = new JButton("=");
    public static JTextField textPanel = new JTextField("0", 10);
    public static JTextField logText = new JTextField("", 10);
    
  

    public  void setLogText(String text) {
        logText.setText(text);
    }

    private GridBagConstraints gbc = new GridBagConstraints();

    public CalculatorView(CalculatorController c) {
        this.cc = c;
        gbc.fill = GridBagConstraints.BOTH;
        Font font = new Font("Helvetica", Font.PLAIN, 18);
        textPanel.setFont(font);
        textPanel.setEditable(false);
        logText.setFont(font);
        logText.setEditable(false);
        setLayout(new GridBagLayout());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 600);
        setLocationRelativeTo(this);

        int counter = 1;
        int x = 3;
        int y = 3;

        for (int i = 3; i > 0; i--) {
            for (int j = 0; j < 3; j++) {
                gbc.gridx = j;
                gbc.gridy = i;
                gbc.gridheight = 1;
                gbc.gridwidth = 1;
                gbc.weightx = 1;
                gbc.weighty = 1;
                gbc.fill = GridBagConstraints.BOTH;

                JButton button = new JButton(String.valueOf(counter++));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cc.handleNumberButtonClick(button.getText ());
                    }
                });
                add(button, gbc);

            }
        }

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 4;
        add(textPanel);
        
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = 4;
        add(logText);

        gbc.gridx = 4;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        add(clearAll, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridheight = 1;
        gbc.gridwidth = 2;
        add(zero, gbc);

        gbc.gridx = 4;
        gbc.gridy = 3;
        gbc.gridheight = 2;
        gbc.gridwidth = 1;
        add(equals, gbc);
//   // clearAll.addActionListener(e -> { controller.handleClearButtonClick();

        gbc.gridx = 4;
        gbc.gridy = 2;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        add(plus, gbc);
        
        gbc.gridx = 2;
        gbc.gridy = 4;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        add(minus, gbc);

        this.equals.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cc.equalsButton();
            }
        });
        
        this.zero.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cc.handleNumberButtonZero();
            }
        });

        this.plus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cc.plusButton();
            }
        });
        this.minus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cc.minusButton();
            }
        });
        this.clearAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cc.allClearButton();
            }
        });

        setVisible(true);

    }

}
