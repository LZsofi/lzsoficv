
package model;

/**
 *
 * @author Zsófi
 */

public class CalculatorModel {
    public enum Operation {
    MINUS,
    PLUS,
    EQUALED,
    UNDEFINED
}
    private String actualNumber;
    private String numberInMemory;
    private Operation operator;


    public CalculatorModel() {
        actualNumber = "";
        numberInMemory = "";
        operator = Operation.UNDEFINED;

    }

    public String getActualNumber() {
        return actualNumber;
    }

    public void setActualNumber(String actualNumber) {
        this.actualNumber = actualNumber;
    }

    public String getNumberInMemory() {
        return numberInMemory;
    }

    public void setNumberInMemory(String numberInMemory) {
        this.numberInMemory = numberInMemory;
    }



    public Operation getOperator() {
        return operator;
    }

    public void setOperator(Operation operator) {
        this.operator = operator;
    }

    

    
}
