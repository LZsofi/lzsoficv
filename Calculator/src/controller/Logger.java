package controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Logger {

    private File log;
    private PrintWriter writer;

    private static Logger INSTANCE;

    private Logger() throws IOException {
        log = new File("C:\\GIt\\lohonyai-zsofia\\bh09_03_31\\GriedBagLayout\\src\\controller\\output.txt");
    }

    public static Logger getInstance() {
        if (INSTANCE == null) {
            try {
                INSTANCE = new Logger();
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return INSTANCE;
    }

    public void writeLine(String line) {
        try {
            writer = new PrintWriter(new FileWriter(log, true));
            writer.println(line);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        

    }

}
