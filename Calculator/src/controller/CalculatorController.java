/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.CalculatorModel;
import model.CalculatorModel.Operation;
import service.CalculatorService;
import view.CalculatorView;
import static view.CalculatorView.logText;

public class CalculatorController {

    private CalculatorModel model;
    private CalculatorView view;
    private CalculatorService calcService;
    private String logLine;

    public CalculatorController() {
        this.model = new CalculatorModel();
        this.view = new CalculatorView(this);
        this.calcService = new CalculatorService();
    }

    public void viewTextPanel(String a) {
        view.textPanel.setText(a);
    }

    public void handleNumberButtonClick(String value) {
        if (model.getOperator() == Operation.EQUALED) {
            allClearButton();
            model.setOperator(Operation.UNDEFINED);
        }
        model.setActualNumber(model.getActualNumber() + value);
        viewTextPanel(model.getActualNumber());
        System.out.println(String.valueOf(value));
        
    }

    public void plusButton() {
        doOperation(Operation.PLUS);
        System.out.println("+");
       
    }

    private void doOperation(Operation operator) {
        if (!model.getActualNumber().equals("")) {
            if (model.getOperator() != Operation.UNDEFINED) {
                equalsButton();
            }
            model.setNumberInMemory(model.getActualNumber());
            model.setActualNumber("");
            model.setOperator(operator);
        }

    }

    public void minusButton() {
        doOperation(Operation.MINUS);
        System.out.println("-");
        
    }

    public void equalsButton() {
        if (!model.getOperator().equals(Operation.UNDEFINED) && !model.getOperator().equals(Operation.EQUALED)) {
            int numberInMemory = Integer.parseInt(model.getNumberInMemory());
            int actualNumber = Integer.parseInt(model.getActualNumber());
            logLine = model.getNumberInMemory();
            int result = 0;
            if (model.getOperator().equals(Operation.PLUS)) {
                result = calcService.addition(numberInMemory, actualNumber);
                logLine += "+";
            } else if (model.getOperator().equals(Operation.MINUS)) {
                result = calcService.subtraction(numberInMemory, actualNumber);
                logLine += "-";
            }
            logLine += model.getActualNumber() + "=" + String.valueOf(result);
            
            Logger.getInstance().writeLine(logLine);
            view.setLogText(logLine);
            
            viewTextPanel(String.valueOf(result));
            model.setActualNumber(String.valueOf(result));
            model.setNumberInMemory("");
            model.setOperator(Operation.EQUALED);
            System.out.println("equals");
            
        }

    }

    public void allClearButton() {
        viewTextPanel(view.zero.getText());
        model.setNumberInMemory("");
        model.setActualNumber("");
        System.out.println("clear");
    }

    public void handleNumberButtonZero() {
        if (!model.getActualNumber().equals("")) {
            model.setActualNumber(model.getActualNumber() + "0");
            viewTextPanel(model.getActualNumber());
            System.out.println("0");
            
        }
    }

}
