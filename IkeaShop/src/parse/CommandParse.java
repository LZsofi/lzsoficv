/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parse;

import backup.Backup;
import Exception.MyException;
import ikea.Barcode;
import ikea.CosmetologyMachines;
import ikea.EntertainmentTools;
import ikea.KitchenTools;
import ikea.TechnicalCommodity;
import java.util.HashMap;
import repo.ProductRepository;
import type.BarcodeType;
import type.TechnicalType;

/**
 *
 * @author Zsófi
 */
public class CommandParse {

    ProductRepository repo = new ProductRepository();

    HashMap<String, TechnicalType> techTypeParser = new HashMap<>();

    public CommandParse() {
        this.techTypeParser.put("CHEAP", TechnicalType.CHEAP);
        this.techTypeParser.put("AVERAGE", TechnicalType.AVERAGE);
        this.techTypeParser.put("EXPENSIVE", TechnicalType.EXPENSIVE);
        this.techTypeParser.put("LUXURY", TechnicalType.LUXURY);
        this.techTypeParser.put("POPULAR", TechnicalType.POPULAR);

    }

    public void parse(String commandLine) throws Exception {
        String[] commands = commandLine.split(" ");

        if (commands.length < 1) {
            throw new Exception("Not enough arguments");
        } else if ("SAVE".equalsIgnoreCase(commands[0])) {
            Backup.save(repo);
            System.out.println("KitchenTools: " + repo.getKitchenToolCount());
            System.out.println("EntertainmentTools: " + repo.getEntertainmentToolCount());
            System.out.println("CosmetologyMachines: " + repo.getCosmetologyMachineCount());
            System.out.println("Good Tools: " + repo.getGoodToolCount());

        } else if ("LOAD".equalsIgnoreCase(commands[0])) {
            repo = Backup.load();

        } else if ("REMOVE".equalsIgnoreCase(commands[0])) {
            
            try {
                repo.sell(repo.getBarcode(commands[1]));
            } catch (MyException e) {

            }
        } else if ("ADD".equalsIgnoreCase(commands[0])) {
            if ("KONYHAI".equalsIgnoreCase(commands[5])) {
                repo.add(new KitchenTools(new Barcode(commands[1]), techTypeParser.get(commands[2]), commands[3], Integer.parseInt(commands[4])));
            } else if ("SZORAKOZTATO".equalsIgnoreCase(commands[5])) {
                repo.add(new EntertainmentTools(new Barcode(commands[1]), techTypeParser.get(commands[2]), commands[3], Integer.parseInt(commands[4])));
            } else if ("SZEPSEGAPOLAS".equalsIgnoreCase(commands[5])) {
                repo.add(new CosmetologyMachines(new Barcode(commands[1]), techTypeParser.get(commands[2]), commands[3], Integer.parseInt(commands[4]), Integer.parseInt(commands[5])));

            }
        }
    }

}
