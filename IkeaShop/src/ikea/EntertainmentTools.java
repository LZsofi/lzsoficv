/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ikea;

import Exception.MyException;
import type.TechnicalType;

/**
 *
 * @author Zsófi
 */
public class EntertainmentTools extends TechnicalCommodity implements Switchable{
    int count = 0; 
    public EntertainmentTools(Barcode barcode, TechnicalType type, String manufacturer, int price) {
         super(barcode, type, manufacturer, price);
    }
    
    @Override
    public void turnOn() throws MyException {
        if (count > 4) {
            throw new MyException("Cannot be turned on more then 5 times!");
        } else {
            System.out.println("Turnable on."); 
            count++;
        }
        
    }
    
    publicint getCount () {
        return count;
    }
    
}
