/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ikea;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import type.TechnicalType;

/**
 *
 * @author Zsófi
 */
public abstract class TechnicalCommodity implements Comparable<TechnicalCommodity>, Serializable{

    TechnicalType type;
    String manufacturer;
    int price;
    Barcode barcode;
    LocalDate date;

    public TechnicalCommodity(Barcode barcode, TechnicalType type, String manufacturer, int price) {
        this.type = type;
        this.manufacturer = manufacturer;
        this.price = price;
        this.barcode = barcode;
        date = LocalDate.now();
    }

    public TechnicalType getType() {
        return type;
    }

    public void setType(TechnicalType type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    public void setBarcode(Barcode barcode) {
        this.barcode = barcode;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.type);
        hash = 47 * hash + Objects.hashCode(this.manufacturer);
        hash = 47 * hash + this.price;
        hash = 47 * hash + Objects.hashCode(this.barcode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TechnicalCommodity other = (TechnicalCommodity) obj;
        if (this.price != other.price) {
            return false;
        }
        if (!Objects.equals(this.manufacturer, other.manufacturer)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.barcode, other.barcode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TechnicalCommodity{" + "type=" + type + ", manufacturer=" + manufacturer + ", price=" + price + ", barcode=" + barcode + '}';
    }

    public int compareTo(TechnicalCommodity otherCommodity) {
        return barcode.number.compareTo(otherCommodity.getBarcode().getNumber());
    }

}
