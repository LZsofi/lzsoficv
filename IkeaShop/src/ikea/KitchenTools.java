/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ikea;

import type.TechnicalType;

/**
 *
 * @author Zsófi
 */
public class KitchenTools extends TechnicalCommodity implements Liftable, Switchable{

    public KitchenTools(Barcode barcode, TechnicalType type, String manufacturer, int price) {
        super(barcode, type, manufacturer, price);
    }

    
    @Override
    public void turnOn() {
        System.out.println("Turnable on.");
    }
    
    @Override
    public void lift() {
        System.out.println("Raiseable.");
    }

    
}
