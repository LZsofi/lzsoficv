/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ikea;

import java.io.Serializable;
import java.util.Objects;
import type.BarcodeType;

/**
 *
 * @author Zsófi
 */
public class Barcode implements Serializable{
    BarcodeType type;
    String number;
    static int count = 1;
    
    public Barcode(String number) {
        this.type = count++ % 2 == 0 ? BarcodeType.DIGITAL : BarcodeType.PRINTED;
        this.number = number;
    }

    public BarcodeType getType() {
        return type;
    }

    public void setType(BarcodeType type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.type);
        hash = 89 * hash + Objects.hashCode(this.number);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Barcode other = (Barcode) obj;
        if (!Objects.equals(this.number, other.number)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Barcode{" + "type=" + type + ", number=" + number + '}';
    }
    
    
}
