/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ikea;

import type.TechnicalType;

/**
 *
 * @author Zsófi
 */
public class CosmetologyMachines extends TechnicalCommodity{
    int weight;

    public CosmetologyMachines(Barcode barcode, TechnicalType type, String manufacturer, int price, int weight) {
        super(barcode, type, manufacturer, price);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.weight;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CosmetologyMachines other = (CosmetologyMachines) obj;
        if (this.weight != other.weight) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CosmetologyMachines{" + "weight=" + weight + '}';
    }
    
    
}
