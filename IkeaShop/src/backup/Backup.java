/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backup;

import ikea.TechnicalCommodity;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import repo.ProductRepository;

/**
 *
 * @author Zsófi
 */
public class Backup {
     private static final String FILE_NAME = "ikea_backup";
    
    public static void save(ProductRepository repo) {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
            o.writeObject(repo);
        } catch (Exception e) {
            e.printStackTrace();
        }     
    }
    
    
    public static ProductRepository load() {
        ProductRepository rep = null;
        
         try (ObjectInputStream i = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
            rep = (ProductRepository) i.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } 
         return rep;
    }
}
