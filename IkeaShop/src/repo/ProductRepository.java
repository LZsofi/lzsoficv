/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repo;

import Exception.MyException;
import ikea.Barcode;
import ikea.CosmetologyMachines;
import ikea.EntertainmentTools;
import ikea.KitchenTools;
import ikea.TechnicalCommodity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Zsófi
 */
public class ProductRepository implements Serializable{
    private List<TechnicalCommodity> list = new ArrayList<>();
        
    public void add(TechnicalCommodity a) {
        list.add(a);
    }
    
    public void sell(Barcode a) {
        TechnicalCommodity result = null;
        for (TechnicalCommodity commodity : list) {
            if (commodity.getBarcode().equals(a)) {
                result = commodity;
                break;
            }
        }
        if (result != null)
            list.remove(result);
    }

    public Barcode getBarcode(String number) throws MyException {
        for (TechnicalCommodity commodity : list) {
            if (commodity.getBarcode().getNumber ().equals(number)) {
                return commodity.getBarcode();
                
            }
        }
        throw new MyException ("Barcode does not exists!");
    }
    
    public int getKitchenToolCount () {
        int result = 0;
        for (TechnicalCommodity commodity : list) {
            if (commodity.getClass () == KitchenTools.class) {
                result++;               
            }
        }
        return result;
    }
    
    public int getEntertainmentToolCount () {
        int result = 0;
        for (TechnicalCommodity commodity : list) {
            if (commodity.getClass () == EntertainmentTools.class) {
                result++;               
            }
        }
        return result;
    }
    
    public int getGoodToolCount () {
        int result = 0;
        for (TechnicalCommodity commodity : list) {
            if (commodity.getClass () == EntertainmentTools.class && ((EntertainmentTools) commodity).getCount () < 4) {
                result++;               
            }
        }
        return result + getKitchenToolCount();
    }
    
    public int getCosmetologyMachineCount () {
        int result = 0;
        for (TechnicalCommodity commodity : list) {
            if (commodity.getClass () == CosmetologyMachines.class) {
                result++;               
            }
        }
        return result;
    }
    
}