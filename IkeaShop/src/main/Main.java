package main;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import parse.CommandParse;

public class Main {

    public static void main(String[] args) {
        CommandParse parser = new CommandParse();

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 10; i++) {

            try {
                parser.parse(scanner.nextLine());
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
